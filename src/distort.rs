// Implements distortion/undistortion to an image. 
extern crate image;
extern crate nalgebra;
extern crate crossbeam;
use self::nalgebra::{ Matrix3, Point2, Vector3};
use std;
use std::f32;
//use self::image;

pub struct Undistort {
    pub focal_length : f32,
    pub roll : f32,
    pub pitch : f32,
    pub yaw : f32,
    pub cx : f32,
    pub cy : f32
}

impl Undistort {
    //Applies undistortion to inBuf and writes to outBuf.
    fn undistort_calculation(&self, src_dim : &Point2<u32>, dest_dim : &Point2<u32>, 
                                                src_pts : &Vec<Point2<u32>>, mapping : &Matrix3<f32>) 
                                                -> Vec<(Point2<u32>, Point2<f32>)>
    {
        let cx2 : f32 = dest_dim.x as f32 / 2.0;
        let cy2 : f32 = dest_dim.y as f32 / 2.0;
        let in_w1 : f32 = std::cmp::min(src_dim.x, src_dim.y) as f32;
        let out_w1 : f32 = std::cmp::min(dest_dim.x, dest_dim.y) as f32;
        let mut result : Vec<(Point2<u32>, Point2<f32>)> = Vec::new();
        for pt in src_pts {
            let x : f32 = (pt.x as f32 - cx2) / out_w1;
            let y : f32 = (pt.y as f32 - cy2) / out_w1;
            let v = Vector3::new(x, y, 1.0);
            let v_u = mapping * v;
            let x_u = v_u[0];
            let y_u = v_u[1];
            let z_u = v_u[2];
            let d : f32 = (x_u * x_u + y_u * y_u).sqrt();
            if z_u > 0.0 && d > 0.0 { 
                let x_d = x_u / d * self.focal_length * (d).atan2(z_u);
                let y_d = y_u / d * self.focal_length * (d).atan2(z_u);
                result.push((*pt, Point2::<f32>::new(x_d * in_w1 + self.cx,
                                                   y_d * in_w1 + self.cy)));        
            } else {
                result.push((*pt, Point2::<f32>::new(-1.0, -1.0)));        
            }
        }
        result
    }

    //Applies undistortion to inBuf and writes to outBuf.
    fn undistort_calculation_stereographic(&self, src_dim : &Point2<u32>, dest_dim : &Point2<u32>, 
                                                src_pts : &Vec<Point2<u32>>, mapping : &Matrix3<f32>) 
                                                -> Vec<(Point2<u32>, Point2<f32>)>
    {
        let cx2 : f32 = dest_dim.x as f32 / 2.0;
        let cy2 : f32 = dest_dim.y as f32 / 2.0;
        let in_w1 : f32 = std::cmp::max(src_dim.x, src_dim.y) as f32;
        let out_w1 : f32 = std::cmp::max(dest_dim.x, dest_dim.y) as f32;
        let mut result : Vec<(Point2<u32>, Point2<f32>)> = Vec::new();
        for pt in src_pts {
            let x : f32 = (pt.x as f32 - cx2) / out_w1;
            let y : f32 = (pt.y as f32 - cy2) / out_w1;
            let v = Vector3::new(x, y, 1.0);
            let v_u = mapping * v;
            let x_u = v_u[0];
            let y_u = v_u[1];
            let z_u = v_u[2];
            let d : f32 = (x_u * x_u + y_u * y_u + z_u * z_u).sqrt();
            if z_u > 0.0 && d > 0.0 { 
                let x_d = x_u  / (d + z_u) * self.focal_length;
                let y_d = y_u  / (d + z_u) * self.focal_length;
                result.push((*pt, Point2::<f32>::new(x_d * in_w1 + self.cx,
                                                   y_d * in_w1 + self.cy)));        
            } else {
                result.push((*pt, Point2::<f32>::new(-1.0, -1.0)));        
            }
        }
        result
    }


    fn calc_mat(&self) -> Matrix3<f32> {
        let cos_roll : f32 = self.roll.cos();
        let sin_roll : f32 = self.roll.sin();
        let cos_pitch : f32 = self.pitch.cos();
        let sin_pitch : f32 = self.pitch.sin();
        let cos_yaw : f32 = self.yaw.cos();
        let sin_yaw : f32 = self.yaw.sin();
        let rot_x = Matrix3::new(1.0, 0.0, 0.0,
                                 0.0, cos_pitch, -sin_pitch,
                                 0.0, sin_pitch, cos_pitch );
        let rot_y = Matrix3::new(cos_yaw, 0.0, sin_yaw,
                                 0.0, 1.0, 0.0,
                                 -sin_yaw, 0.0, cos_yaw );
        let rot_z = Matrix3::new(cos_roll, -sin_roll, 0.0,
                                 sin_roll, cos_roll, 0.0,
                                 0.0, 0.0, 1.0 );
        rot_z * rot_x * rot_y
    }

    pub fn undistort <T, C>(&self, in_buf : &image::ImageBuffer<T, C>, out_buf : &mut image::ImageBuffer<T,C>)
    -> Vec<(Point2<u32>,Point2<f32>)>
    where T: image::Pixel + std::convert::From<image::Rgb<u8>> + 'static,
          C: std::ops::Deref<Target = [T::Subpixel]> + std::ops::DerefMut,
          image::Rgb<u8>: std::convert::From<image::Rgb<<T as image::Pixel>::Subpixel>>,
          T::Subpixel: 'static
    {
        let r_mat =  self.calc_mat();
        let gen_pts = |height : u32, width : u32| -> Vec<Point2<u32>> {
            let mut result : Vec<Point2<u32>> = Vec::new();
            for py in 0..height {
                for px in 0..width {
                    result.push(Point2::<u32>::new(px, py));
                }
            }
            result
        };
        let in_pts = gen_pts(out_buf.height(), out_buf.width());
        let threads : usize = std::cmp::min(4, (out_buf.height() * out_buf.width()) as usize); 
        let in_dim = Point2::<u32>::new(in_buf.width(), in_buf.height());
        let out_dim = Point2::<u32>::new(out_buf.width(), out_buf.height());
        let mut out_pts : Vec<(Point2<u32>,Point2<f32>)> = Vec::new();
        let mut handles : Vec<self::crossbeam::ScopedJoinHandle<Vec<(Point2<u32>,Point2<f32>)>>> = Vec::new();
        self::crossbeam::scope(|sc| { 
            for ch in in_pts.chunks(in_pts.len() / threads) {
                handles.push(sc.spawn( move || -> Vec<(Point2<u32>,Point2<f32>)> {
                    self.undistort_calculation_stereographic(&in_dim, &out_dim, &(ch.to_vec()), &r_mat)
                }));
            }
            for i in handles {
                out_pts.append(&mut i.join());
            }
        });                      
        for (in_pt, out_pt) in out_pts.clone() {
            out_buf.put_pixel(in_pt.x, in_pt.y, self.bilinear(in_buf,
                                out_pt.x,
                                out_pt.y));        
        }
        return out_pts;
    }

    //Applies distortion to inBuf and writes to outBuf.
    pub fn distort<T, C>(&self, in_buf : &image::ImageBuffer<T,C>, out_buf : &mut image::ImageBuffer<T,C>)
    where T: image::Pixel + std::convert::From<image::Rgb<u8>> + 'static,
          C: std::ops::Deref<Target = [T::Subpixel]> + std::ops::DerefMut,
          image::Rgb<u8>: std::convert::From<image::Rgb<<T as image::Pixel>::Subpixel>>,
          T::Subpixel: 'static
    {
        for y in 0..out_buf.height() {
            for x in 0..out_buf.width() {
                out_buf.put_pixel(x, y, self.bilinear(in_buf, x as f32, y as f32));        
            }
        }
    }

    //Get resulting pixel value from a bilinear interpoaled read from inBuf.
    fn bilinear <T, C>(&self, in_buf : &image::ImageBuffer<T,C>, x : f32, y : f32) -> T
    where T: image::Pixel + std::convert::From<image::Rgb<u8>> + 'static,
          C: std::ops::Deref<Target = [T::Subpixel]> + std::ops::DerefMut,
          image::Rgb<u8>: std::convert::From<image::Rgb<<T as image::Pixel>::Subpixel>>,
          T::Subpixel: image::Primitive + 'static
    {
        if !x.is_finite() || !y.is_finite() ||
        x < 0.0 || y < 0.0 || x > in_buf.width() as f32 - 2.0 || y > in_buf.height() as f32 - 2.0 {
            let dummy = image::Rgb { data: [0 as u8, 0, 0] };
            return dummy.into();
        }
        let x1 = x.floor() as u32;
        let y1 = y.floor() as u32;
        let x2 = x1 + 1;
        let y2 = y1 + 1;
        let pix11 : image::Rgb<u8>= in_buf.get_pixel(x1, y1).to_rgb().into();
        let pix12 : image::Rgb<u8>= in_buf.get_pixel(x2, y1).to_rgb().into();
        let pix21 : image::Rgb<u8>= in_buf.get_pixel(x1, y2).to_rgb().into();
        let pix22 : image::Rgb<u8>= in_buf.get_pixel(x2, y2).to_rgb().into();
        let w1 : f32 = x - (x1 as f32);
        let w2 : f32 = y - (y1 as f32);
        let mut rgb = image::Rgb { data: [0 as u8, 0, 0] };
       for chan in 0..3 as usize {
            rgb.data[chan] = (pix11.data[chan] as f32 * (1.0 - w1) * (1.0 - w2) 
                                    + pix12.data[chan] as f32 * w1 * (1.0 - w2) 
                                    + pix21.data[chan] as f32 * (1.0 - w1) * w2 
                                    + pix22.data[chan] as f32 * w1 * w2) as u8;
        }
        return rgb.into();
    }
}
