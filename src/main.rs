extern crate image;
mod distort;
use std::io::prelude::Write;
use std::io::BufWriter;
use std::fs::File;
use std::path::Path;
use image::{GenericImage};


fn main() {
    //Input parameters.
    

    let in_img = image::open(&Path::new("image.jpg")).ok().expect("Failed opening file.");
    let (width, height) = in_img.dimensions();
    let parameters1 = distort::Undistort{
        focal_length : 1.0 / 3.3,
        roll : 0.0,
        pitch : 0.0,
        yaw : 0.0,
        cx : in_img.width() as f32 / 2.0,
        cy : in_img.height() as f32 / 2.0
   };
    let parameters2 = distort::Undistort{
        focal_length : 1.0 / 3.3,
        roll : 3.14 * 0.5,
        pitch : 0.0,
        yaw : 0.0,
        cx : in_img.width() as f32 / 2.0,
        cy : in_img.height() as f32 / 2.0
    };
    let parameters3 = distort::Undistort{
        focal_length : 1.0 / 3.3,
        roll : 0.0,
        pitch : 3.14 * 0.5,
        yaw : 0.0,
        cx : in_img.width() as f32 / 2.0,
        cy : in_img.height() as f32 / 2.0
    };
    let parameters4 = distort::Undistort{
        focal_length : 1.0 / 3.3,
        roll : 0.0,
        pitch : 0.0,
        yaw : 3.14 * 0.5,
        cx : in_img.width() as f32 / 2.0,
        cy : in_img.height() as f32 / 2.0
    };
    let parameters5 = distort::Undistort{
        focal_length : 1.0 / 3.3,
        roll : 0.0,
        pitch : -3.14 * 0.5,
        yaw : 0.0,
        cx : in_img.width() as f32 / 2.0,
        cy : in_img.height() as f32 / 2.0
    };
    let parameters6 = distort::Undistort{
        focal_length : 1.0 / 3.3,
        roll : 0.0,
        pitch : 0.0,
        yaw : -3.14 * 0.5,
        cx : in_img.width() as f32 / 2.0,
        cy : in_img.height() as f32 / 2.0
    };

    // Saving distorted image.
    match in_img.as_rgb8() {
        Some(x) =>
        {
         //   let mut distorted = image::ImageBuffer::<image::Rgb<u8>, Vec<u8> >::new(width, height);
            let mut undistorted1 = image::ImageBuffer::<image::Rgb<u8>, Vec<u8> >::new(640, 480);
            let mut undistorted2 = image::ImageBuffer::<image::Rgb<u8>, Vec<u8> >::new(640, 480);
            let mut undistorted3 = image::ImageBuffer::<image::Rgb<u8>, Vec<u8> >::new(640, 480);
            let mut undistorted4 = image::ImageBuffer::<image::Rgb<u8>, Vec<u8> >::new(640, 480);
            let mut undistorted5 = image::ImageBuffer::<image::Rgb<u8>, Vec<u8> >::new(640, 480);
            let mut undistorted6 = image::ImageBuffer::<image::Rgb<u8>, Vec<u8> >::new(640, 480);
         //parameters.distort(x, &mut distorted);
            let pts1 = parameters1.undistort(x, &mut undistorted1);
            let pts2 = parameters2.undistort(x, &mut undistorted2);
            let pts3 = parameters3.undistort(x, &mut undistorted3);
            let pts4 = parameters4.undistort(x, &mut undistorted4);
            let pts5 = parameters5.undistort(x, &mut undistorted5);
            let pts6 = parameters6.undistort(x, &mut undistorted6);

            image::ImageRgb8(undistorted1).save(&Path::new("undistImg1.jpg")).unwrap();
            image::ImageRgb8(undistorted2).save(&Path::new("undistImg2.jpg")).unwrap();
            image::ImageRgb8(undistorted3).save(&Path::new("undistImg3.jpg")).unwrap();
            image::ImageRgb8(undistorted4).save(&Path::new("undistImg4.jpg")).unwrap();
            image::ImageRgb8(undistorted5).save(&Path::new("undistImg5.jpg")).unwrap();
            image::ImageRgb8(undistorted6).save(&Path::new("undistImg6.jpg")).unwrap();
            
            let mut writer1 = BufWriter::new(File::create(&Path::new("golden.txt")).expect("Cannot create file."));
            let mut writer_param = BufWriter::new(File::create(&Path::new("golden.param.txt")).expect("Cannot create file"));
            for (p1, p2) in pts1 {
                writeln!(&mut writer1, " {} {} {} {}", p1.x, p1.y, p2.x, p2.y);
            }
            writeln!(&mut writer_param, "focal_length:{}", parameters1.focal_length);
            writeln!(&mut writer_param, "roll:{}", parameters1.roll);
            writeln!(&mut writer_param, "pitch:{}", parameters1.pitch);
            writeln!(&mut writer_param, "yaw:{}", parameters1.yaw);
            writeln!(&mut writer_param, "cx:{}", parameters1.cx);
            writeln!(&mut writer_param, "cy:{}", parameters1.cy);
            writeln!(&mut writer_param, "source_width:{}", in_img.width());
            writeln!(&mut writer_param, "source_height:{}", in_img.height());
            writeln!(&mut writer_param, "dest_width:{}", 640);
            writeln!(&mut writer_param, "dest_height:{}", 480);
           //image::ImageRgb8(distorted).save(&Path::new("distImg.jpg")).unwrap();
        }
        None => {println!("Input image cannot be converted to RGB8!")}
    }
}